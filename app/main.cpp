#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <stdlib.h>
#include <stdio.h>

#include <vector>
#include "input.hpp"
#include "fpscounter.hpp"
#include "gamemanager.hpp"

#include <iostream>

std::shared_ptr<GameManager> gm;
//siut::FpsCounter counter;
siut::Clock timer;

int window;

bool keyPressed[30];
int mousePosX, mousePosY; 
float moveX, moveY;

GLenum err = GL_NO_ERROR;

void glErrorCheck(std::string s)
{
  // Nothing to render

  err = glGetError();
  if (err != GL_NO_ERROR )
      std::cout << "OpenGL error (" << s << "): "
                << err << " " << gluErrorString(err) << std::endl;
}


void init()
{
  std::cout << "OpenGL version: " << glGetString( GL_VERSION ) << "\n";
  std::cout << "Vendor string : " << glGetString( GL_VENDOR ) << "\n";
  std::cout << "Renderer      : " << glGetString( GL_RENDERER) << std::endl;
  std::cout << "Shader version: " << glGetString( GL_SHADING_LANGUAGE_VERSION ) << std::endl;
  glClearColor(0.1f, 0.1f, 0.3f, 0.0);

  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);

  timer.start();

  gm.reset(new GameManager());
  gm->init();

  for(int i=0; i<30; i++)
    keyPressed[i]=false;


}

void display()
{

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  //gm->update(counter.fps());
  gm->update(timer.lap());

  gm->render();

  if(keyPressed[KEY_ID_W]==true)      gm->getCam()->moveForward();
  if(keyPressed[KEY_ID_A]==true)      gm->getCam()->moveLeft();
  if(keyPressed[KEY_ID_D]==true)      gm->getCam()->moveRight();
  if(keyPressed[KEY_ID_S]==true)      gm->getCam()->moveBackward();
  if(keyPressed[KEY_ID_SPACE]==true)  gm->getCam()->moveUp();
  if(keyPressed[KEY_ID_C]==true)      gm->getCam()->moveDown();

  if(keyPressed[KEY_ID_LEFT]==true)      gm->getPlayer()->moveLeft();
  if(keyPressed[KEY_ID_RIGHT]==true)      gm->getPlayer()->moveRight();
  if(keyPressed[KEY_ID_UP]==true)      gm->getPlayer()->moveForward();
  if(keyPressed[KEY_ID_DOWN]==true)      gm->getPlayer()->moveBackward();

  if(keyPressed[KEY_ID_Z]==true)      gm->fireMissile();


  glutSwapBuffers();
  glutPostRedisplay();


}

void keyDown(unsigned char key, int x, int y)
{
  switch (key) 
  {
    case 'q':
    case 27:
      glutDestroyWindow(window);
#ifndef _WIN32
      // Must use this with regular glut, since it never returns control to main().
      exit(0);
#endif
      break;
      
    case 'w':
      keyPressed[KEY_ID_W] = true;
      break;
    case 'a':
      keyPressed[KEY_ID_A] = true;
      break;
    case 's':
      keyPressed[KEY_ID_S] = true;
      break;
    case 'd':
      keyPressed[KEY_ID_D] = true;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE] = true;
      break;
    case 'c':
      keyPressed[KEY_ID_C] = true;
      break;
      /*ijkl exchanged for arrow keys
    case 'j':
      keyPressed[KEY_ID_J] = true;
      break;
    case 'l':
      keyPressed[KEY_ID_L] = true;
      break;
    case 'i':
      keyPressed[KEY_ID_I] = true;
      break;
    case 'k':
      keyPressed[KEY_ID_K] = true;
    break;
    */
    case 'z':
      keyPressed[KEY_ID_Z] = true;
      break;

    default:
      glutPostRedisplay();
  }
}

void keyUp(unsigned char key, int x, int y)
{
  switch (key) 
  {
    case 'w':
      keyPressed[KEY_ID_W] = false;
      break;
    case 'a':
      keyPressed[KEY_ID_A] = false;
      break;
    case 's':
      keyPressed[KEY_ID_S] = false;
      break;
    case 'd':
      keyPressed[KEY_ID_D] = false;
      break;
    case ' ':
      keyPressed[KEY_ID_SPACE] = false;
      break;
    case 'c':
      keyPressed[KEY_ID_C] = false;
      break;
      /*Exchanged for arrow key movement
    case 'j':
      keyPressed[KEY_ID_J] = false;
      break;
    case 'l':
      keyPressed[KEY_ID_L] = false;
      break;
    case 'i':
      keyPressed[KEY_ID_I] = false;
      break;
    case 'k':
      keyPressed[KEY_ID_K] = false;
      break;
      */
    case 'z':
      keyPressed[KEY_ID_Z] = false;
      break;
  }
}

void specialKeyDown(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_UP:
            keyPressed[KEY_ID_UP] = true;
            std::cout << "Up pressed" << std::endl;
        break;
        case GLUT_KEY_DOWN:
            keyPressed[KEY_ID_DOWN] = true;
            std::cout << "Down pressed" << std::endl;
        break;
        case GLUT_KEY_LEFT:
            keyPressed[KEY_ID_LEFT] = true;
            std::cout << "Left pressed" << std::endl;
        break;
        case GLUT_KEY_RIGHT:
            keyPressed[KEY_ID_RIGHT] = true;
            std::cout << "Right pressed" << std::endl;
        break;
    }
}

void specialKeyUp(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_UP:
            keyPressed[KEY_ID_UP] = false;
        break;
        case GLUT_KEY_DOWN:
            keyPressed[KEY_ID_DOWN] = false;
        break;
        case GLUT_KEY_LEFT:
            keyPressed[KEY_ID_LEFT] = false;
        break;
        case GLUT_KEY_RIGHT:
            keyPressed[KEY_ID_RIGHT] = false;
        break;
    }
}

void mousePressed(int button, int state, int posX, int posY)
{
  if(button==GLUT_LEFT_BUTTON && state==GLUT_DOWN)
  {
    mousePosX = posX;
    mousePosY = posY;
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = true;
  }  
  if(button==GLUT_LEFT_BUTTON && state==GLUT_UP)
    keyPressed[MOUSE_LEFT_BUTTON_DOWN] = false;
}

void mouseMoved(int posX, int posY)
{
  if(keyPressed[MOUSE_LEFT_BUTTON_DOWN])
  {
    int diffX = posX - mousePosX;
    mousePosX = posX;
    int diffY = posY - mousePosY;
    mousePosY = posY;
    
    // Implement quaternion based mouse move

  }

}

void reshape(int w, int h)
{
  glErrorCheck("reshape, start");
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);
  gm.get()->reshape(w,h);
  glErrorCheck("reshape, end");
}

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
  glutInitWindowSize(700, 700); 
  glutInitWindowPosition(10, 10);
  glutInitContextVersion (4, 4);


  window = glutCreateWindow("Space Shooter 3D");

  glewExperimental = GL_TRUE;
  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    std::cout << "GLEW error: " << glewGetErrorString(err) << std::endl;
  }
  glErrorCheck("GLEW - This is OK");//Bug in GLEW but this can aparently be ignored after reading the error
  init();


  glutKeyboardFunc(keyDown);
  glutKeyboardUpFunc(keyUp);
  glutSpecialFunc(specialKeyDown);
  glutSpecialUpFunc(specialKeyUp);
  glutReshapeFunc(reshape);
  glutDisplayFunc(display);
  glutMouseFunc(mousePressed);
  glutMotionFunc(mouseMoved);


  // Add other callback functions here..

  glutMainLoop();
  return 0;
}
