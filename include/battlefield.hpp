#pragma once
#define WIDTH 32
#define DEPTH 512
#define SCALE 10

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "sceneobject.hpp"
#include "imagedata.hpp"
#include "camera.hpp"
#include "lightsource.hpp"

class BattleField : public SceneObject
{
public:
    struct LightparamID
    {
        GLuint pos;
        GLuint ints;
        GLuint diff;
        GLuint amb;
    };

    BattleField();
    ~BattleField();

    void addTexture(const GLuint &texture);

    GLint getProgramID() const;
    void setProgramID(const GLint &value);

protected:
    virtual void privateInit();
    virtual void privateRender();
    virtual void privateUpdate(double dt);

private:
    std::vector< float > vertexArray_;
    std::vector< uint > indexArray_;
    std::vector< float > uvArray_;
    std::vector< float > uvClutterArray_;


    GLuint vertexArrayID;
    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint uvBuffer;
    GLuint uvClutterBuffer;


    //Should strictly be GLuint, but handy to set to -1 for primitive error checking.
    GLint programID = -1;
    GLint matrixID = -1; //MVP
    GLint modelID = -1; //M
    GLint viewID = -1; //V

    std::vector< GLuint> textures;
    GLint textureID = -1;
    GLint colormapID = -1;
    GLint terrainclutterID = -1;
    GLint normalmapID = -1;

    LightparamID lightID;
};
