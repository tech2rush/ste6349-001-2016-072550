#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "sceneobject.hpp"
#include <vector>

class Camera : public SceneObject
{
	public:
		Camera();
		~Camera();

    void moveRight();
    void moveLeft();
    void moveUp();
    void moveDown();
    void moveBackward();
    void moveForward();

    void reshape(int w, int h);

    glm::mat4 projectionMat;
    glm::mat4 viewMat = glm::mat4();

    float getCamSpeed() const;
    void setCamSpeed(float value);

protected:
    void privateInit();
    void privateRender();
    void privateUpdate(double dt);

private:
    float camSpeed_ = 2.0f;

};


