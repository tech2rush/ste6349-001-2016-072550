#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include "GL/glew.h"

#include <glm/glm.hpp>
#include <vector>

#include "model.hpp"


class CubeModel : public Model
{
public:
    CubeModel();
    ~CubeModel();
    void init() override;
    void prepare() override;
    void render() override;
private:
};
