#ifndef ENEMY_HPP
#define ENEMY_HPP

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "sceneobject.hpp"
#include "camera.hpp"
#include "model.hpp"

class Enemy : public SceneObject
{
    public:
        Enemy();
        ~Enemy();

        float speed() const;
        void setSpeed(float speed);

        float ttl() const;
        void setTtl(float ttl);

        GLuint getProgramID() const;
        void setProgramID(const GLuint &value);

        void applyDamage(int damage);
        float getLife() const;
        void setLife(float life);

        std::shared_ptr<Model> getModel() const;
        void setModel(const std::shared_ptr<Model> &model);

protected:
        void privateInit();
        void privateRender();
        void privateUpdate(double dt);

private:
    float speed_ = 10.0f;
    float life_ = 100.0f;
    float armor_ = 0.0f;
    float ttl_ = 20.0f;//time to live before auto removal

    std::shared_ptr<Model> model_;

    GLint programID = -1;
    GLint matrixID = -1;

};

#endif
