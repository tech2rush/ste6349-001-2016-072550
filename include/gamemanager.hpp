#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <glm/gtc/matrix_transform.hpp>
#include <png.h>
#include <string.h>
#include <iostream>
#include <list>

#include "sceneobject.hpp"
#include "battlefield.hpp"
#include "spaceship.hpp"
#include "enemy.hpp"
#include "camera.hpp"
#include "imagedata.hpp"
#include "missile.hpp"
#include "skybox.hpp"

class GameManager : public SceneObject
{
public:
    GameManager();
    ~GameManager();
    
    std::shared_ptr<SpaceShip> getPlayer();

    void fireMissile();

    void reshape(int w, int h);

protected:
    virtual void privateInit() override;
    virtual void privateRender() override;
    virtual void privateUpdate(double dt) override;

    //Texture and shader IDs
    //Note that these are hardcoded sizes simply to save time, should be
    //vector of texture_info structs or objects.
    GLuint textures[10];
    GLuint shaders[10];

    std::shared_ptr<BattleField> bf_;
    std::shared_ptr<SpaceShip> spaceship_;
    std::shared_ptr<Skybox> skybox_;
    std::list< std::shared_ptr<Enemy> > enemyList_;
    std::list< std::shared_ptr<Missile> > missileList_;

    std::shared_ptr<Model> cubeModel;
    float timeSinceLastSpawn;
    int maxEnemies_ = 5;

    void handleCollisions();
    std::shared_ptr<Enemy> test;
};

#endif
//Control idea: auto-shooting, left-right click barrel roll to avoid shots (level change)
