#ifndef IMAGEDATA_H
#define IMAGEDATA_H

#include <GL/gl.h>
#include <GL/glu.h>

class ImageData
{
public:
    int height;
    int width;
    bool hasAlpha;
    GLubyte *rawImageData;
};

#endif
