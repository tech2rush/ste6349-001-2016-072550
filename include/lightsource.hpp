#ifndef LIGHTSOURCE_HPP
#define LIGHTSOURCE_HPP

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "sceneobject.hpp"
#include <glm/glm.hpp>
#include "camera.hpp"

class Lightsource : public SceneObject
{
public:
    struct LightParam
    {
        glm::vec3 position;
        GLfloat intensity;
        glm::vec3 diffuse;
        GLfloat ambience;
    };

   Lightsource(){};
    ~Lightsource(){};

   LightParam getLightParam() const;
   void setLightParam(const LightParam &value);

   void translate(glm::vec3 tVec);

protected:
   void privateInit() override;
   void privateRender() override;
   void privateUpdate(double dt) override;

   Lightsource::LightParam lightParam =
    {

        glm::vec3(0.0f, 0.0f, 0.0f),
        1000.0f,
        glm::vec3(1.0f, 1.0f, 1.0f),
        0.4f
   };
};

#endif
