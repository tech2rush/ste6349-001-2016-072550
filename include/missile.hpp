#ifndef MISSILE_HPP
#define MISSILE_HPP

#ifdef _WIN32
#include <windows.h>
#endif

//#include <GL/glew.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "sceneobject.hpp"
#include <glm/glm.hpp>
#include "camera.hpp"
#include "model.hpp"

class Missile : public SceneObject
{
public:
    Missile();
    ~Missile();

    float speed() const;
    void setSpeed(float speed);

    float ttl() const;
    void setTtl(float ttl);

    GLuint getProgramID() const;
    void setProgramID(const GLuint &value);

    int getDamage() const;
    void setDamage(int value);

    std::shared_ptr<Model> getModel() const;
    void setModel(const std::shared_ptr<Model> &model);

protected:
    void privateInit();
    void privateRender();
    void privateUpdate(double dt);

private:
    float speed_ = 40.0f;
    float ttl_ = 4.0f;//time to live
    int damage = 100;

    std::shared_ptr<Model> model_;

    GLint programID = -1;
    GLint vertexArrayID = -1;
    GLint vertexbuffer = -1;
    GLint matrixID = -1;

};

#endif
