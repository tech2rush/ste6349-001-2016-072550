#pragma once

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include "lightsource.hpp"

class Model
{
public:
    struct LightparamID
    {
        GLuint pos;
        GLuint ints;
        GLuint diff;
        GLuint amb;
    };

    Model();
    ~Model();
    void init();
    void render();

    glm::vec3 getColor() const;
    void setColor(float r, float g, float b);
    void setColor(const glm::vec3 &value);
    void addTexture(const GLuint &texture);
    bool loadObjFile(char *file);
protected:
    float size = 1.0f;
    glm::vec3 color_ = glm::vec3(0.25f, 0.5f, 1.0f);
    std::vector< GLuint> textures_;
    LightparamID lightID_;

    //Should strictly be GLuint, but handy to set to -1 for primitive error checking.
    GLint programID_ = -1;
    GLint matrixID_ = -1; //MVP
    GLint modelID_ = -1; //M
    GLint viewID_ = -1; //V

    //Buffer IDS
    GLuint vertexbuffer_;
    GLuint uvbuffer_;

    //Model data
    std::vector< glm::vec3 > vertices_;
    std::vector< glm::vec2 > uvs_;
    std::vector< glm::vec3 > normals_;
};

