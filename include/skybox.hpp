#ifndef SKYBOX_HPP
#define SKYBOX_HPP

#ifdef _WIN32
#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "sceneobject.hpp"
#include "camera.hpp"

//Based on https://www.keithlantz.net/2011/10/rendering-a-skybox-using-a-cube-map-with-opengl-and-glsl/
//+various other sources. Original example was not core profile
//Using large skybox per https://www.khronos.org/opengl/wiki/Skybox but with z-hack

class Skybox : public SceneObject
{
public:
    Skybox();
    ~Skybox();

    std::vector<GLuint> getTextures() const;
    void setTextures(const std::vector<GLuint> &textures);
    void addTexture(const GLuint texture);

    GLuint getProgramID() const;
    void setProgramID(const GLuint &programID);

    GLuint getMatrixID() const;
    void setMatrixID(const GLuint &matrixID);

protected:
    void privateInit();
    void privateRender();
    void privateUpdate(double dt);

private:
    std::vector<GLuint> textures_;
    GLuint programID_;
    GLuint vertexArrayID_;
    GLuint vertexbuffer_;
    GLuint matrixID_;
    GLuint cubemapID_;

    GLuint vertexBufferLocation;

    GLuint ibo_cube_indices_;
    GLuint vbo_cube_vertices_;

    GLuint elementsCount_;
    bool testmode_ = false;
};

#endif
