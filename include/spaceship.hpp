#ifndef SPACESHIP_HPP
#define SPACESHIP_HPP

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "sceneobject.hpp"
#include "camera.hpp"
#include "model.hpp"


class SpaceShip : public SceneObject
{
	public:
        SpaceShip();
        ~SpaceShip();

        void moveLeft();
        void moveRight();
        void moveForward();
        void moveBackward();

        float getMissileCooldown() const;
        void setMissileCooldown(float value);
        bool missileReady();
        void fireMissile();

        float getMovespeed() const;
        void setMovespeed(float movespeed);

        GLuint getProgramID() const;
        void setProgramID(const GLuint &value);

        std::shared_ptr<Model> getModel() const;
        void setModel(const std::shared_ptr<Model> &model);

protected:
        void privateInit();
        void privateRender();
        void privateUpdate(double dt);
private:
    float movespeed_ = 0.5f;
    float speed_;
    float life_;
    float armor_;
    float missileCooldown_ = 0.2f;
    float timeSinceLastMissile_ = 0.0f;

    std::shared_ptr<Model> model_;

    GLint programID = -1;//For assert
    GLint matrixID = -1;
};

#endif
