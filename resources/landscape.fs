#version 330 core

struct Lightparams
{
    vec3 position; 
    float intensity;
    vec3 diffuse; 
    float ambience;
};

in vec2 UV;
in vec2 cUV;

//Light related
in vec3 pos_worldspace;
in vec3 normal_camspace;
in vec3 eyeDir_camspace;
in vec3 lightDir_camspace;

in float waterlevel;
in vec3 vertexPos;

out vec3 color;

uniform sampler2D heightSampler;
uniform sampler2D colormapSampler;
uniform sampler2D clutterSampler;
uniform sampler2D normalmapSampler;

uniform Lightparams light;

void main()
{
    if (vertexPos.y < waterlevel)
    {
	//Do water stuff
	color = vec3(0.0f, 0.5f, 0.9f);
    }
    else
    {

    vec3 clutterSample = texture2D(clutterSampler, cUV).rgb;
    
    //If using height/steepness mapping. Works but using textures from assignment.
    //float height = texture2D( heightSampler, UV).r;
    //float steepness = dot ( vec3(0.0f, 1.0f, 0.0f), texture2D( normalmapSampler, UV).rgb); 
    //vec3 materialDiffuseColor = texture2D( colormapSampler, vec2(height, steepness )).rgb;

    vec3 materialDiffuseColor = texture2D(colormapSampler, UV).rgb;

    if (clutterSample.x - clutterSample.y < 0.1 ) //Hack to detect the magenta cutout.
    {
        materialDiffuseColor = mix(materialDiffuseColor, clutterSample, 0.3);
    }

    vec3 materialAmbientColor = materialDiffuseColor * light.ambience;
    vec3 materialSpecularColor = vec3(0.3,0.3,0.3);

    float distance = length (light.position - pos_worldspace);

    //Positional light
    //vec3 n = normalize( normal_camspace);
    //vec3 l = normalize( lightDir_camspace);
    //l = normalize( vec3(1.0f, -1.0f, 1.0f));
    //float cosTheta = clamp( dot( n, l ), 0, 1);

    //Directional light
    vec3 m = 2 * texture2D( normalmapSampler, UV).rgb - vec3(1.0f, 1.0f, 1.0f);
    vec3 n = normalize(m);
    vec3 l = normalize(vec3(0.4f, -1.0f, 0.0f)); 
    float cosTheta = clamp( dot( n,l ), 0, 1);

    //Reflection
    vec3 eye = normalize( eyeDir_camspace );
    vec3 reflection = reflect(-l, n);
    float cosAlpha = clamp( dot( eye, reflection ), 0, 1);
    
    color = 
        materialAmbientColor +
        materialDiffuseColor * light.diffuse * cosTheta * 1.25;
        // * light.intensity * cosTheta / (distance * distance)
        // + materialSpecularColor * light.diffuse;
    }
}
