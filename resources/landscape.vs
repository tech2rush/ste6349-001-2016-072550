#version 330 core

struct Lightparams
{
    vec3 position; 
    float intensity;
    vec3 diffuse; 
    float ambience;
};

layout(location = 0) in vec3 vertexPos_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec2 clutterUV;

//Texture 
out vec2 UV;
out vec2 cUV;

//Light related
out vec3 pos_worldspace;
out vec3 normal_camspace;
out vec3 eyeDir_camspace;
out vec3 lightDir_camspace;

out float waterlevel;

uniform mat4 MVP;
uniform mat4 M;
uniform mat4 V;
uniform sampler2D heightSampler;
uniform sampler2D normalmapSampler;

uniform Lightparams light;

out vec3 vertexPos;

void main()
{
  //Adjust heights according to heightmap
  vertexPos = vertexPos_modelspace +
  	vec3(0, 100 * texture(heightSampler, vertexUV).r, 0);
  //Adjust for water level
  waterlevel = 60; 
  if (vertexPos.y < waterlevel) vertexPos.y = waterlevel -0.1;//Just to avoid == against a float in fs
  gl_Position = MVP * vec4(vertexPos,1);


  //Light related.
  pos_worldspace = (M * vec4(vertexPos_modelspace,1)).xyz;

  vec3 vertexPos_camspace = (V * M * vec4(vertexPos_modelspace,1)).xyz;
  eyeDir_camspace = vec3(0.0f, 0.0f, 0.0f) - vertexPos_camspace;

  vec3 lightPos_camspace = ( V * vec4(light.position,1)).xyz;
  lightDir_camspace = lightPos_camspace + eyeDir_camspace;

  vec3 normal = 2 * texture2D(normalmapSampler, vertexUV).xyz - vec3(1.0f, 1.0f, 1.0f); 
  normal_camspace = (V * M * vec4(normal, 1)).xyz;


  //Pass UV coords.
  UV = vertexUV;
  cUV = clutterUV; 
}
