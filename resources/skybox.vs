#version 330 core

layout(location = 0) in vec3 vertexPos_modelspace;

uniform mat4 MVP;

out vec3 texCoord;

void main()
{
  gl_Position = MVP * vec4(vertexPos_modelspace, 1);
  gl_Position = gl_Position.xyww;
  texCoord = vertexPos_modelspace;
  texCoord.y *= -1; 
}
