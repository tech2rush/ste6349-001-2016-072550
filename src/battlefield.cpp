#include "../include/battlefield.hpp"
#include <iostream>
#include <assert.h>

BattleField::BattleField()
{
}

BattleField::~BattleField()
{
}


void BattleField::privateInit()
{
    float offsetX = - SCALE * ( WIDTH / 2 );
    float offsetZ = - SCALE * ( DEPTH / 2 );

  // Create vertex arrays
    for (int k = 0; k <= DEPTH ; k++)
    {
        for (int i = 0; i <= WIDTH ; i++)
        {
            vertexArray_.emplace_back( offsetX + i * SCALE );
            vertexArray_.emplace_back(0);
            vertexArray_.emplace_back( offsetZ + k * SCALE );
            uvArray_.emplace_back( i / 1.0f / WIDTH);
            uvArray_.emplace_back( k / 1.0f / DEPTH );
            uvClutterArray_.emplace_back(i%2);
            uvClutterArray_.emplace_back(k%2);
        }
    }
  // Create indices

    for (int k = 0; k < DEPTH; k++)
    {
       int i;
       for (i = 0; i <= WIDTH; i++ )
       {
           //Counter-clockwise order
          indexArray_.emplace_back( i + k * ( WIDTH + 1) );
          indexArray_.emplace_back( i + ( k + 1 ) * ( WIDTH + 1 ) );
       }

      if ( k < DEPTH )
      {
        // Create degenerate triangles to mimic glPrimitiveRestart.
        // Triangles with area = 0 are not rendered.
        indexArray_.emplace_back( i - 1 + ( k + 1 ) * ( WIDTH + 1) ); // -1 b/c of for(..;i++)
        indexArray_.emplace_back( 0 + ( k + 1 ) * ( WIDTH + 1) );
      }
    }

    assert(programID > -1);

    glGenVertexArrays(1, &vertexArrayID);

    glBindVertexArray(vertexArrayID);

    //Fill array buffer
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer( GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData( GL_ARRAY_BUFFER,
                  vertexArray_.size() * sizeof(GLfloat),
                  &vertexArray_[0],
                  GL_STATIC_DRAW );

    //Fill uv buffer
    glGenBuffers(1, &uvBuffer);
    glBindBuffer( GL_ARRAY_BUFFER, uvBuffer);
    glBufferData( GL_ARRAY_BUFFER,
                  uvArray_.size() * sizeof(GLfloat),
                  &uvArray_[0],
                  GL_STATIC_DRAW);

    //Fill clutter uv buffer
    glGenBuffers(1, &uvClutterBuffer);
    glBindBuffer( GL_ARRAY_BUFFER, uvClutterBuffer);
    glBufferData( GL_ARRAY_BUFFER,
                  uvClutterArray_.size() * sizeof(GLfloat),
                  &uvClutterArray_[0],
                  GL_STATIC_DRAW);

    //Fill element buffer
    glGenBuffers(1, &indexBuffer);
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                  indexArray_.size() * sizeof(GLuint),
                  &indexArray_[0],
                  GL_STATIC_DRAW );



   //Get uniforms
   matrixID = glGetUniformLocation(programID, "MVP");
   modelID = glGetUniformLocation(programID, "M");
   viewID = glGetUniformLocation(programID, "V");

   textureID = glGetUniformLocation(programID, "textureSampler");
   colormapID = glGetUniformLocation(programID, "colormapSampler");
   terrainclutterID = glGetUniformLocation(programID, "clutterSampler");
   normalmapID = glGetUniformLocation(programID, "normalmapSampler");

   lightID.pos = glGetUniformLocation(programID, "light.position");
   lightID.ints = glGetUniformLocation(programID, "light.intensity");
   lightID.diff= glGetUniformLocation(programID, "light.diffuse");
   lightID.amb = glGetUniformLocation(programID, "light.ambience");

   SceneObject::errorCheck("BattleField::privateInit end.\n");
}

void BattleField::privateRender()
{
    glm::mat4 MVP = cam_->projectionMat * cam_->viewMat * SceneObject::view_stack.top();
    glm::mat4 M = SceneObject::view_stack.top();
    glm::mat4 V = cam_->viewMat;

    glUseProgram(programID);

    SceneObject::errorCheck("BattleField::privateRender 1\n");

    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);

    //Light
    glUniform3f(lightID.pos,
                ls_->getLightParam().position[0],
                ls_->getLightParam().position[1],
                ls_->getLightParam().position[2]);

    glUniform1f(lightID.ints,
                ls_->getLightParam().intensity);
    glUniform3f(lightID.diff,
                ls_->getLightParam().diffuse[0],
                ls_->getLightParam().diffuse[1],
                ls_->getLightParam().diffuse[2]);
    glUniform1f(lightID.amb,
                ls_->getLightParam().ambience);

    //Bind texture

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textures[0]);
    glUniform1i(textureID, 0);
    SceneObject::errorCheck("BattleField::privateRender 3\n");

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, textures[1]);
    glUniform1i(colormapID, 1);
    SceneObject::errorCheck("BattleField::privateRender 4\n");

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, textures[2]);
    glUniform1i(terrainclutterID, 2);
    SceneObject::errorCheck("BattleField::privateRender 4.1\n");

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, textures[3]);
    glUniform1i(normalmapID, 3);
    SceneObject::errorCheck("BattleField::privateRender 4.2\n");

    //Vertices
    glEnableVertexAttribArray(0);
    SceneObject::errorCheck("BattleField::privateRender 5\n");

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    SceneObject::errorCheck("BattleField::privateRender 6\n");

    glVertexAttribPointer(
        0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );

    //UV data
    glEnableVertexAttribArray(1);

    glBindBuffer( GL_ARRAY_BUFFER, uvBuffer);
    SceneObject::errorCheck("BattleField::privateRender 7\n");

    glVertexAttribPointer(
        1,                  // attribute 1. No particular reason for 0, but must match the layout in the shader.
        2,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );
    SceneObject::errorCheck("BattleField::privateRender 8\n");

    //Clutter UV data
    glEnableVertexAttribArray(2);

    glBindBuffer( GL_ARRAY_BUFFER, uvClutterBuffer);
    SceneObject::errorCheck("BattleField::privateRender 9\n");

    glVertexAttribPointer(
        2,                  // attribute 1. No particular reason for 0, but must match the layout in the shader.
        2,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );
    SceneObject::errorCheck("BattleField::privateRender 10\n");


    //Indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glDrawElements( GL_TRIANGLE_STRIP, indexArray_.size() , GL_UNSIGNED_INT, (void*)0 );
    SceneObject::errorCheck("BattleField::privateRender 9\n");

    //Light

    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    SceneObject::errorCheck("BattleField::privateRender 10\n");
}

void BattleField::privateUpdate(double dt)
{
}

GLint BattleField::getProgramID() const
{
    return programID;
}

void BattleField::setProgramID(const GLint &value)
{
    programID = value;
}

void BattleField::addTexture(const GLuint &texture)
{
    textures.emplace_back(texture);
}
