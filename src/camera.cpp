#include "../include/camera.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#define PIOVER180 0.0174532925199

Camera::Camera()
{
  viewMat = glm::lookAt(
              glm::vec3(0.0f, 15.0f, 50.0f),
              glm::vec3(0.0f, 0.0f, 0.0f),
              glm::vec3(0.0f, 1.0f, 0.0f));
  matrix_ = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.0f, 0.0f));
}

Camera::~Camera()
{
}

void Camera::privateInit()
{
}


void Camera::privateRender()
{
  // not drawing any camera geometry
}

void Camera::privateUpdate(double dt)
{

}

float Camera::getCamSpeed() const
{
    return camSpeed_;
}

void Camera::setCamSpeed(float value)
{
    camSpeed_ = value;
}

void Camera::moveRight()
{
    matrix_ = glm::translate(matrix_, glm::vec3(-camSpeed_, 0.0f, 0.0f));
}
void Camera::moveLeft()
{
  matrix_ = glm::translate(matrix_, glm::vec3(camSpeed_, 0.0f, 0.0f));
}
void Camera::moveUp()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, -camSpeed_, 0.0f));
}
void Camera::moveDown()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, camSpeed_, 0.0f));
}
void Camera::moveForward()
{
    matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, camSpeed_));
}

void Camera::moveBackward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -camSpeed_));
}

void Camera::reshape(int w, int h)
{
  projectionMat = glm::perspective(3.14f/3,float(w)/float(h) ,1.0f, 3000.0f );
}


