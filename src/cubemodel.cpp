#include "cubemodel.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/normal.hpp>

#include "texture.hpp"

CubeModel::CubeModel()
{

}

CubeModel::~CubeModel()
{

}

void CubeModel::init()
{
}

void CubeModel::prepare()
{
}

void CubeModel::render()
{

}

