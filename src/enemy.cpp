#include "../include/enemy.hpp"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::privateInit()
{
    assert(programID > -1);
    matrixID = glGetUniformLocation(programID, "MVP");

    SceneObject::errorCheck("Model::prepare, get uniforms");
}

void Enemy::privateRender()
{
    glm::mat4 m = SceneObject::view_stack.top()
            * glm::scale(glm::mat4(), getScale());

    glm::mat4 MVP = cam_->projectionMat * cam_->viewMat * m;//this->matrix_;
    glUseProgram(programID);
    SceneObject::errorCheck("Enemy::privateRender 1\n");

    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);
    model_->render();
}

void Enemy::privateUpdate(double dt)
{
    this->translate(glm::vec3(0.0f, 0.0f, -dt * speed_));
    ttl_ -= dt;
}

std::shared_ptr<Model> Enemy::getModel() const
{
    return model_;
}

void Enemy::setModel(const std::shared_ptr<Model> &model)
{
    model_ = model;
}

float Enemy::getLife() const
{
    return life_;
}

void Enemy::setLife(float life)
{
    life_ = life;
}

GLuint Enemy::getProgramID() const
{
    return programID;
}

void Enemy::setProgramID(const GLuint &value)
{
    programID = value;
}

float Enemy::ttl() const
{
    return ttl_;
}

void Enemy::setTtl(float ttl)
{
    ttl_ = ttl;
}

float Enemy::speed() const
{
    return speed_;
}

void Enemy::setSpeed(float speed)
{
    speed_ = speed;
}

void Enemy::applyDamage(int damage)
{
    setLife(getLife() - damage);
}
