#include "../include/gamemanager.hpp"

#include <cstdlib>
#include <ctime>

#include "loadshaders.hpp"
#include "texture.hpp"
#include <iostream>

GameManager::GameManager()
{
}

GameManager::~GameManager()
{
}

void GameManager::privateInit()
{
  std::srand(std::time(0));//Seed rng with current time
  timeSinceLastSpawn = 10.0f;

  // Set default OpenGL states
  glEnable(GL_CULL_FACE);

  // Adding the camera to the scene
  cam_.reset(new Camera());
  //this->addSubObject(cam_);

  //Load textures
  textures[0] = loadBMP_custom("../resources/P7.bmp");
  textures[1] = loadBMP_custom("../resources/heightMap2012.bmp");
  textures[2] = loadBMP_custom("../resources/colorMap2012.bmp");
  textures[3] = loadBMP_custom("../resources/terrainclutter.bmp");
  textures[4] = loadBMP_custom("../resources/normalMap2012.bmp");

  std::vector<char*> skybox_images =
  {
      (char*)("../resources/skybox/skybox_east.bmp"),
      (char*)("../resources/skybox/skybox_west.bmp"),
      (char*)("../resources/skybox/skybox_down.bmp"),
      (char*)("../resources/skybox/skybox_up.bmp"),
      (char*)("../resources/skybox/skybox_north.bmp"),
      (char*)("../resources/skybox/skybox_south.bmp"),
  };

  textures[5] = loadBMP_cubemap(skybox_images);

  //Load shaders
  shaders[0] = LoadShaders( "../resources/simple.vs",
                            "../resources/simple.fs");
  shaders[1] = LoadShaders( "../resources/landscape.vs",
                            "../resources/landscape.fs");
  shaders[2] = LoadShaders( "../resources/skybox.vs",
                            "../resources/skybox.fs");
  shaders[3] = LoadShaders( "../resources/simplephong.vs",
                            "../resources/simplephong.fs");

  //Load models
  cubeModel.reset(new Model());
  if (cubeModel->loadObjFile((char*)("../resources/cube.obj")))
      cubeModel->init();
  else (exit(1));

  //Set up scene objects.

  ls_.reset(new Lightsource());
  ls_->translate(glm::vec3(0.0f, 1000.0f, 0.0f));


  bf_.reset(new BattleField());
  bf_->setCam(cam_);
  bf_->setLight(ls_);
  bf_->translate(glm::vec3(0.0f, -100.0f, -2500.0f));
  bf_->addTexture(textures[1]);
  bf_->addTexture(textures[2]);
  bf_->addTexture(textures[3]);
  bf_->addTexture(textures[4]);
  bf_->setProgramID(shaders[1]);

  addSubObject(bf_);

  spaceship_.reset(new SpaceShip());
  spaceship_->setCam(cam_);
  spaceship_->setLight(ls_);
  spaceship_->setProgramID(shaders[0]);
  spaceship_->setModel(cubeModel);
  spaceship_->translate(glm::vec3(0.0f, 0.0f, 30.0f));
  addSubObject(spaceship_);

  skybox_.reset(new Skybox());
  skybox_->setCam(cam_);
  skybox_->addTexture(textures[5]);
  skybox_->setProgramID(shaders[2]);
  skybox_->setScale(glm::vec3(100.0f, 100.0f, 100.0f));
  addSubObject(skybox_);


  std::cout << "GameManager init completed." << std::endl;
}

void GameManager::privateRender()
{
  // Nothing to render
}

void GameManager::privateUpdate(double dt)
{
    this->matrix_ = cam_->getMatrix();

    //Move battlefield to simulate continous spaceship movement
    float movedistance = 10.0f * (float)dt;
    bf_->translate(glm::vec3(0.0f, 0.0f, 1.0f) * movedistance);

    //Check && handle collisions. Crude but sufficient for a 2d game.
    handleCollisions();

    //Handle enemy spawning. Should be delegated to a separate controller for larger project.
    if (enemyList_.size() < maxEnemies_ && timeSinceLastSpawn > 2.5f)
    {
        std::cout << "Spawning new enemy! ";
        std::shared_ptr<Enemy> enemy;
        enemy.reset(new Enemy());
        float xPos = std::rand() * 1.0f / RAND_MAX * 64.0f -32.0f;
        enemy->setCam(cam_);
        enemy->setLight(ls_);
        enemy->setProgramID(shaders[0]);
        enemy->setModel(cubeModel);
        enemy->translate(glm::vec3(xPos, 0.0f, -100.0f));
        enemy->rotate(3.1415f, glm::vec3(0.0f, 1.0f, 0.0f));
        enemy->init();
        addSubObject(enemy);

        enemyList_.emplace_back(enemy);
        timeSinceLastSpawn = 0.0f;

        std::cout << enemyList_.front().get()->ttl() << "s before timeout.\n";

        std::cout << "  Number of enemies is now: " << enemyList_.size() << std::endl;
    }
    else timeSinceLastSpawn += dt;

    //Check initfor enemies that have timed out
   for( std::list< std::shared_ptr<Enemy> >::iterator it = enemyList_.begin();
         it != enemyList_.end(); it++)
    {
        if ((*it).get()->ttl() < 0)
        {
            std::cout << "An enemy has timed out.\n";
            this->removeSubObject((*it));
            it = enemyList_.erase(it);
            std::cout << "  Number of enemies is now: " << enemyList_.size() << std::endl;
        }
   }

   //Check for missiles that have timed out
   for( std::list< std::shared_ptr<Missile> >::iterator it = missileList_.begin();
         it != missileList_.end(); it++)
    {
        if ((*it).get()->ttl() < 0)
        {
            std::cout << "A missile has timed out.\n";
            this->removeSubObject((*it));
            it = missileList_.erase(it);
            std::cout << "  Number of missiles is now: " << missileList_.size() << std::endl;
        }
    }
}

std::shared_ptr<SpaceShip> GameManager::getPlayer()
{
    return spaceship_;

}

void GameManager::fireMissile()
{
    if (!spaceship_->missileReady()) return;
    spaceship_->fireMissile();
    std::cout << "Launching missile!\n";
    std::shared_ptr<Missile> m;
    m.reset(new Missile());
    m->setCam(cam_);
    m->setLight(ls_);
    m->setProgramID(shaders[0]);
    m->setModel(cubeModel);
    m->setScale(glm::vec3(0.25f, 0.25f, 0.25f));
    m->translate(spaceship_->getGlobalPos());
    m->init();
    this->addSubObject(m);
    missileList_.emplace_back(m);
    std::cout << "  Number of missiles is now: " << missileList_.size() << std::endl;
}

void GameManager::reshape(int w, int h)
{
    cam_.get()->reshape(w,h);

}

void GameManager::handleCollisions()
{
    //Super simple 2D collision detection
   for( std::list< std::shared_ptr<Enemy> >::iterator eit = enemyList_.begin(); eit != enemyList_.end(); eit++)
   {
        for( std::list< std::shared_ptr<Missile> >::iterator mit = missileList_.begin(); mit != missileList_.end(); mit++)
        {
            glm::vec3 enemyPos = (*eit)->getGlobalPos();
            glm::vec3 missilePos = (*mit)->getGlobalPos();
            //hardcoding r = 5 for circle collision, for now
            glm::vec3 dVector = enemyPos - missilePos;

            if ( glm::length(dVector) < 5.0f) //Collision
            {
                (*mit)->setTtl(0);
                (*eit)->applyDamage((*mit)->getDamage());
                if ((*eit)->getLife() <= 0) (*eit)->setTtl(0);
                std::cout << "It's a hit!" << std::endl;
            }
        }
   }
}

