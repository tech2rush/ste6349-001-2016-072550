#include "lightsource.hpp"

void Lightsource::privateInit()
{
}

void Lightsource::privateRender()
{

}

void Lightsource::privateUpdate(double dt)
{

}

Lightsource::LightParam Lightsource::getLightParam() const
{
    return lightParam;
}

void Lightsource::setLightParam(const Lightsource::LightParam &value)
{
    lightParam = value;
}

void Lightsource::translate(glm::vec3 tVec)
{
    glm::mat4 tMat = glm::translate(
                glm::mat4(),
                tVec);
    matrix_ *= tMat;
    lightParam.position += tVec;
}
