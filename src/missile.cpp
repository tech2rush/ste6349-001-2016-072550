#include "missile.hpp"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "loadshaders.hpp"

Missile::Missile()
{
}

Missile::~Missile()
{
}

void Missile::privateInit()
{
   // Create and compile our GLSL program from the shaders
    assert(programID > -1);
    matrixID = glGetUniformLocation(programID, "MVP");

    SceneObject::errorCheck("Model::prepare, get uniforms");
}

void Missile::privateRender()
{
    glm::mat4 m = SceneObject::view_stack.top()
            * glm::scale(glm::mat4(), getScale());

    glm::mat4 MVP = cam_->projectionMat * cam_->viewMat * m; //this->matrix_;
    // Use our shader
    glUseProgram(programID);
    SceneObject::errorCheck("Enemy::privateRender 1\n");

    // Pass MVP matrix
    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);
    model_->render();
}

void Missile::privateUpdate(double dt)
{
    this->translate(glm::vec3(0.0f, 0.0f, -dt * speed_));
    ttl_ -= dt;

}

std::shared_ptr<Model> Missile::getModel() const
{
    return model_;
}

void Missile::setModel(const std::shared_ptr<Model> &model)
{
    model_ = model;
}

int Missile::getDamage() const
{
    return damage;
}

void Missile::setDamage(int value)
{
    damage = value;
}

GLuint Missile::getProgramID() const
{
    return programID;
}

void Missile::setProgramID(const GLuint &value)
{
    programID = value;
}

float Missile::ttl() const
{
    return ttl_;
}

void Missile::setTtl(float ttl)
{
    ttl_ = ttl;
}

float Missile::speed() const
{
    return speed_;
}

void Missile::setSpeed(float speed)
{
    speed_ = speed;
}
