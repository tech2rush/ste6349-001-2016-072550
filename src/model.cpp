#include "model.hpp"
#include "objloader.hpp"
#include <iostream>

Model::Model()
{

}

Model::~Model()
{

}

glm::vec3 Model::getColor() const
{
    return color_;
}

void Model::setColor(float r, float g, float b)
{
    color_ = glm::vec3(r,g,b);
}

void Model::setColor(const glm::vec3 &value)
{
    color_ = value;
}
void Model::addTexture(const GLuint &texture)
{
    textures_.emplace_back(texture);
}

bool Model::loadObjFile(char *file)
{
    bool r = loadOBJ(file, vertices_, uvs_, normals_);
    if (!r) std::cout << "Could not load: " << file << std::endl;
    return r;
}

void Model::init()
{
   //Upload data to OpenGL
    glGenBuffers(1, &vertexbuffer_);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
    glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(glm::vec3), &vertices_[0], GL_STATIC_DRAW);

    glGenBuffers(1, &uvbuffer_);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer_);
    glBufferData(GL_ARRAY_BUFFER, uvs_.size() * sizeof(glm::vec2), &uvs_[0], GL_STATIC_DRAW);
}

void Model::render()
{


    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer_);
    glVertexAttribPointer(
                0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                3,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer_);
    glVertexAttribPointer(
                1,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                2,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void*)0            // array buffer offset
                );


    glDrawArrays(GL_TRIANGLES, 0, vertices_.size());

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);
}
