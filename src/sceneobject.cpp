#include "../include/sceneobject.hpp"

//#include <windows.h>
#include <iostream>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"


std::stack <glm::mat4> SceneObject::view_stack;

// NB! Check matrix mult and scoped_ptr

SceneObject::SceneObject()
{
  //setIdentity(matrix_);
  matrix_ = glm::mat4();
  if (SceneObject::view_stack.size() == 0)
      SceneObject::view_stack.push(glm::mat4(1.0f));
}

SceneObject::~SceneObject()
{
}

void SceneObject::render()
{
  SceneObject::errorCheck("SceneObject::render\n");
  //glPushMatrix();
  SceneObject::view_stack.push(SceneObject::view_stack.top() * matrix_);
  this->privateRender();
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
    it != children_.end(); it++)
    (*it)->render();
  SceneObject:view_stack.pop();
}

void SceneObject::update(double dt)
{

  this->dt_ = dt;
  this->privateUpdate(dt);
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
      it != children_.end(); it++)
      (*it)->update(dt);

}

void SceneObject::init()
{

  this->privateInit();
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
      it != children_.end(); it++)
      (*it)->init();

}

void SceneObject::addSubObject(std::shared_ptr<SceneObject> newchild)
{
  children_.push_back(newchild);
}

void SceneObject::removeSubObject(const std::shared_ptr<SceneObject> child)
{
  for(std::vector<std::shared_ptr<SceneObject> >::iterator it = children_.begin();
      it != children_.end(); it++)
    if(*it == child)
    {
      children_.erase(it);
      break;
    }
}

void SceneObject::rotate(float angle, glm::vec3 rot_axle)
{
   glm::mat4 rMat = glm::rotate(glm::mat4(),angle,rot_axle );
   matrix_ *=  rMat;
}

void SceneObject::translate(glm::vec3 tVec)
{
    glm::mat4 tMat = glm::translate(glm::mat4(),tVec);
    matrix_ *= tMat;
}

std::shared_ptr<Camera> SceneObject::getCam() const
{
    return cam_;
}

void SceneObject::setCam(const std::shared_ptr<Camera> &cam)
{
    cam_ = cam;
}

glm::vec3 SceneObject::getGlobalPos()
{
    //Hack
    return glm::vec3(matrix_[3][0], matrix_[3][1], matrix_[3][2]);
}

GLenum SceneObject::errorCheck(std::string s)
{
  GLenum err;// = glGetError();
  int i = 0;
  do
  {
      err = glGetError();
      if (err != GL_NO_ERROR)
      std::cout << i++ << " -> OpenGL error (" << s << "): "
                << "(" << err << ") (" << gluErrorString(err) << ")" << std::endl;
  }while (err != GL_NO_ERROR);

  return err;
}

std::shared_ptr<Lightsource> SceneObject::getLs() const
{
    return ls_;
}

void SceneObject::setLight(const std::shared_ptr<Lightsource> &ls)
{
    ls_ = ls;
}

glm::vec3 SceneObject::getScale() const
{
    return scale_;
}

void SceneObject::setScale(const glm::vec3 &scale)
{
    scale_ = scale;
}
