#include "skybox.hpp"
#include <iostream>

Skybox::Skybox()
{

}

Skybox::~Skybox()
{

}


void Skybox::privateInit()
{
    GLfloat cube_vertices[] = {
        -1.0,  1.0,  1.0,
        -1.0, -1.0,  1.0,
        1.0, -1.0,  1.0,
        1.0,  1.0,  1.0,
        -1.0,  1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,
        1.0,  1.0, -1.0,
    };

   //Separate triangles
    GLushort cube_indices[] = {
        3,7,2,7,6,2,//X+
        0,1,4,1,5,4,//X-
        0,4,3,4,7,3,//Y+
        1,2,5,2,6,5,//Y-
        4,5,7,5,6,7,//Z-
        0,3,2,0,2,1,//Z+
    };

    elementsCount_ = sizeof(cube_indices)/sizeof(GLushort);

    vbo_cube_vertices_;
    glGenBuffers(1, &vbo_cube_vertices_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);

    ibo_cube_indices_;
    glGenBuffers(1, &ibo_cube_indices_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_cube_indices_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(cube_indices), cube_indices, GL_STATIC_DRAW);


    matrixID_ = glGetUniformLocation(programID_, "MVP");
    vertexBufferLocation = glGetAttribLocation(programID_, "vertexPos_modelspace");
    cubemapID_ = glGetUniformLocation(programID_, "cubemap");

}

void Skybox::privateRender()
{
    glm::mat4 m = SceneObject::view_stack.top()
            * glm::scale(glm::mat4(), getScale());
    glm::mat4 MVP = cam_->projectionMat * cam_->viewMat * m;

    glUseProgram(programID_);
    glUniformMatrix4fv(matrixID_,1,GL_FALSE, &MVP[0][0]);
    SceneObject::errorCheck("Skybox::privateRender, uniform passing");

    glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_vertices_);
    glEnableVertexAttribArray(vertexBufferLocation);
    glVertexAttribPointer(vertexBufferLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
    SceneObject::errorCheck("Skybox::privateRender, vertex binding");

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_cube_indices_);
    SceneObject::errorCheck("Skybox::privateRender, index binding");

    glActiveTexture(GL_TEXTURE0);
    SceneObject::errorCheck("Skybox::privateRender, texure binding1");
    glBindTexture(GL_TEXTURE_CUBE_MAP, textures_[0]);
    SceneObject::errorCheck("Skybox::privateRender, texure binding2");
    glUniform1i(cubemapID_, 0);
    SceneObject::errorCheck("Skybox::privateRender, texure binding3");
    SceneObject::errorCheck("Skybox::privateRender, texure binding");

    glDepthFunc(GL_LEQUAL);
    glDrawElements(GL_TRIANGLES, elementsCount_, GL_UNSIGNED_SHORT, 0);
    glDepthFunc(GL_LESS);
    SceneObject::errorCheck("Skybox::privateRender, drawElements");

    glDisableVertexAttribArray(vertexBufferLocation);
    SceneObject::errorCheck("Skybox::privateRender, end");
}

void Skybox::privateUpdate(double dt)
{
}

GLuint Skybox::getMatrixID() const
{
    return matrixID_;
}

void Skybox::setMatrixID(const GLuint &matrixID)
{
    matrixID_ = matrixID;
}


std::vector<GLuint> Skybox::getTextures() const
{
    return textures_;
}

void Skybox::setTextures(const std::vector<GLuint> &textures)
{
    textures_ = textures;
}

void Skybox::addTexture(const GLuint texture)
{
    textures_.push_back(texture);
}

GLuint Skybox::getProgramID() const
{
    return programID_;
}

void Skybox::setProgramID(const GLuint &programID)
{
    programID_ = programID;
}

