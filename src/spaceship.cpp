
#include "spaceship.hpp"
#include "loadshaders.hpp"
#include "objloader.hpp"
#include <iostream>

SpaceShip::SpaceShip()
{
}

SpaceShip::~SpaceShip()
{
}

void SpaceShip::privateInit()
{
    assert(programID > -1);
    matrixID = glGetUniformLocation(programID, "MVP");

    SceneObject::errorCheck("Model::prepare, get uniforms");
}

void SpaceShip::privateRender()
{
    glm::mat4 m = SceneObject::view_stack.top()
            * glm::scale(glm::mat4(), getScale());

    glm::mat4 MVP = cam_->projectionMat * cam_->viewMat * m;//this->matrix_;
    glUseProgram(programID);
    SceneObject::errorCheck("Enemy::privateRender 1\n");

    glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MVP[0][0]);
    model_->render();
}

void SpaceShip::privateUpdate(double dt)
{
    timeSinceLastMissile_ += dt;
}

std::shared_ptr<Model> SpaceShip::getModel() const
{
    return model_;
}

void SpaceShip::setModel(const std::shared_ptr<Model> &model)
{
    model_ = model;
}

GLuint SpaceShip::getProgramID() const
{
    return programID;
}

void SpaceShip::setProgramID(const GLuint &value)
{
    programID = value;
}

float SpaceShip::getMovespeed() const
{
    return movespeed_;
}

void SpaceShip::setMovespeed(float movespeed)
{
    movespeed_ = movespeed;
}

float SpaceShip::getMissileCooldown() const
{
    return missileCooldown_;
}

void SpaceShip::setMissileCooldown(float value)
{
    missileCooldown_ = value;
}

bool SpaceShip::missileReady()
{
   if (timeSinceLastMissile_ > missileCooldown_) return true;
   return false;
}

void SpaceShip::fireMissile()
{
   timeSinceLastMissile_ = 0;
}

void SpaceShip::moveRight()
{
    matrix_ = glm::translate(matrix_, glm::vec3(1.0f, 0.0f, 0.0f) * getMovespeed());
}

void SpaceShip::moveLeft()
{
    matrix_ = glm::translate(matrix_, glm::vec3(-1.0f, 0.0f, 0.0f) * getMovespeed());
}

void SpaceShip::moveForward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, -1.0f) * getMovespeed());
}

void SpaceShip::moveBackward()
{
  matrix_ = glm::translate(matrix_, glm::vec3(0.0f, 0.0f, 1.0f) * getMovespeed());
}
